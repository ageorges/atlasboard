![Atlasboard](https://bitbucket.org/atlassian/atlasboard/raw/81f89ba6f63020cef294a55bf8b833c79e68b98f/atlasboard.jpg)


##Installation##

`npm install -g atlasboard`


##Creating your first wallboard##

After installing Atlasboard as a global module, you may want to do this:

`atlasboard new mywallboard`


to generate your wallboard.


For more information check out the [Atlasboard website](http://atlasboard.bitbucket.org).
=

